package src;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by nmatushe
 **/

public class Reading
{
	private static Reading t = null;
	private static BufferedWriter bufferedWriter = null;

	private Reading() 
	{
	}

	public static Reading gett()
	{
		if (t == null)
			t = new Reading();
		if (bufferedWriter == null)
		{
			try
			{
				bufferedWriter = new BufferedWriter(new FileWriter(new File("simulation.txt")));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return t;
	}

	public void close()
	{
		try
		{
			bufferedWriter.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public void log(String str)
	{
		try
		{
			bufferedWriter.write(str);
			bufferedWriter.newLine();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}


}