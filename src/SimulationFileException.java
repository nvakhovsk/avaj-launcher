package src;

/**
 * Created by nmatushe
 **/

public class SimulationFileException extends AvajLauncherException 
{
    public SimulationFileException()
    {
        super();
    }

    public SimulationFileException(String message)
    {
        super(message);
    }
}
