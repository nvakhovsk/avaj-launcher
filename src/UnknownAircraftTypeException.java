package src;

/**
 * Created by nmatushe
 **/

public class UnknownAircraftTypeException extends AvajLauncherException
{
    public UnknownAircraftTypeException()
    {
        super();
    }

    public UnknownAircraftTypeException(String message)
    {
        super(message);
    }
}
