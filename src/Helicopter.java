package src;

import src.UnknownWeatherException;
import src.WeatherTower;
import src.Reading;

/**
 * Created by nmatushe
 **/

public class Helicopter extends Aircraft implements Flyable
{
	private WeatherTower weatherTower;

	Helicopter(String name, Coordinates coordinates)
	{
		super(name, coordinates);
		this.weatherTower = null;
	}

	public void updateConditions() throws UnknownWeatherException
	{
		String weather = this.weatherTower.getWeather(super.coordinates);
		Coordinates newCoordinates = null;

		switch (weather)
		{
			case "SUN":
				newCoordinates = new Coordinates(super.coordinates.getLongitude() + 10,
													super.coordinates.getLatitude(),
													Math.min(super.coordinates.getHeight() + 2, 100));
				Reading.gett().log("Helicopter#" + super.name + "(" + super.id + "): This is hot.");
				break;
			case "RAIN":
				newCoordinates = new Coordinates(super.coordinates.getLongitude() + 5,
													super.coordinates.getLatitude(),
													super.coordinates.getHeight());
				Reading.gett().log("Helicopter#" + super.name + "(" + super.id + "): It's raining.");
				break;
			case "FOG":
				newCoordinates = new Coordinates(super.coordinates.getLongitude() + 1,
													super.coordinates.getLatitude(),
													super.coordinates.getHeight());
				Reading.gett().log("Helicopter#" + super.name + "(" + super.id + "): It's so foggy.");
				break;
			case "SNOW":
				newCoordinates = new Coordinates(super.coordinates.getLongitude(),
													super.coordinates.getLatitude() ,
													Math.max(super.coordinates.getHeight() - 12, 0));
				Reading.gett().log("Helicopter#" + super.name + "(" + super.id + "): My rotor is going to freeze.");
				break;
			default:
				throw new UnknownWeatherException("Unknown weather: " + weather);
		}

		super.coordinates = newCoordinates;

		if (super.coordinates.getHeight() == 0)
		{
			Reading.gett().log("Helicopter#" + super.name + "(" + super.id + ") landing.");
			this.weatherTower.unregister(this);
			Reading.gett().log("Tower says: Helicopter#" + super.name + "(" + super.id + ") unregistered from weather tower."
			);
		}
	}

	public void registerTower(WeatherTower weatherTower)
	{
		weatherTower.register(this);
		this.weatherTower = weatherTower;
		Reading.gett().log("Tower says: Helicopter#" + super.name + "(" + super.id + ") registered to weather tower.");
	}
}
