package src;

/**
 * Created by nmatushe
 **/

public class InvalidCoordinatesException extends AvajLauncherException
{
    public InvalidCoordinatesException()
    {
        super();
    }

    public InvalidCoordinatesException(String message)
    {
        super(message);
    }
}
