package src;

/**
 * Created by nmatushe
 **/

public class UnknownWeatherException extends AvajLauncherException
{
    public UnknownWeatherException()
    {
        super();
    }

    public UnknownWeatherException(String message)
    {
        super(message);
    }
}
