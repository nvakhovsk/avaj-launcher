package src;

import src.Coordinates;

import java.util.Random;

/**
 * Created by nmatushe
 **/

public class WeatherProvider
{
    private static WeatherProvider weatherProvider = new WeatherProvider();
    private static String[] weather = {"RAIN", "FOG", "SUN", "SNOW"};
    private static int a = 1;
    private static int b = 1;
    private static int c = 1;
    private static Random randomGenerator = new Random();

    private WeatherProvider()
    {
        weatherProvider = this;
    }

    public static WeatherProvider getProvider()
    {
        return weatherProvider;
    }

    public String getCurrentWeather(Coordinates coordinates) 
    {
        return weather[(a * coordinates.getLatitude() + b * coordinates.getLongitude() + c * coordinates.getHeight()) % 4];
    }

    public void changeWeather() 
    {
        a = randomGenerator.nextInt(1111);
        b = randomGenerator.nextInt(1111);
        c = randomGenerator.nextInt(1111);
    }
}