package src;

/**
 * Created by nmatushe
 **/

public abstract class Aircraft
{
	private static long		idCounter = 0;

	protected long			id;
	protected String		name;
	protected Coordinates	coordinates;

	private long nextId()
	{
		return ++idCounter;
	}

	protected Aircraft(String name, Coordinates coordinates)
	{
		this.name = name;
		this.coordinates = coordinates;
		this.id = nextId();
	}
}