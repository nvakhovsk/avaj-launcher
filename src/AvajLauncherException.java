package src;

/**
 * Created by nmatushe
 **/

public class AvajLauncherException extends Exception
{
    public AvajLauncherException()
    {
        super();
    }

    public AvajLauncherException(String message)
    {
        super(message);
    }
}
