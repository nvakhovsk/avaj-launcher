package src;

import src.UnknownWeatherException;
import src.Flyable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nmatushe
 **/

public abstract class Tower
{
	private List<Flyable> observers = new ArrayList<>();

	public void register(Flyable flyable)
	{
		this.observers.add(flyable);
	}

	public void unregister(Flyable flyable)
	{
		this.observers.remove(flyable);
	}

	protected void conditionsChanged() throws UnknownWeatherException
	{
		List<Flyable> copyOfObservers = new ArrayList<>();

		for (Flyable flyable : observers)
		{
			copyOfObservers.add(flyable);
		}

		for (Flyable flyable : copyOfObservers)
		{
			flyable.updateConditions();
		}
	}
}
