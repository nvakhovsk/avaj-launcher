package src;

import src.AvajLauncherException;
import src.SimulationFileException;
import src.AircraftFactory;
import src.Flyable;
import src.Reading;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nmatushe
 **/

public class Sim
{
	private static WeatherTower weatherTower;
	private static List<Flyable> airs = new ArrayList<>();

	public static void main(String[] args)
	{
		if (args.length != 1)
		{
			System.out.println("Usage: scenario.txt");
			System.exit(0);
		}

		try
		{
			File file = new File(args[0]);
			if (!file.exists())
			{
				System.out.println("File " + args[0] + " does not exist.");
				System.exit(1);
			}
			if (file.isDirectory())
			{
				System.out.println(args[0] + " not a file.");
				System.exit(1);
			}

			BufferedReader Reading = new BufferedReader(new FileReader(file));
			String line = Reading.readLine();

			if (line != null)
			{
				weatherTower = new WeatherTower();
				int simulations = 0;
				try
				{
					simulations = Integer.parseInt(line.split(" ")[0]);
				}
				catch (NumberFormatException e)
				{
					throw new SimulationFileException("Invalid simulation file: integer expected.");
				}

				if (simulations < 0)
				{
					System.out.println("Invalid simulation count " + simulations);
					System.exit(1);
				}

				while ((line = Reading.readLine()) != null)
				{
					String[] word = line.split(" ");

					if (word.length != 5)
					{
						throw new SimulationFileException("Invalid simulation file, must be like: TYPE NAME LONGITUDE LATITUDE HEIGHT");
					}

					try
					{
						Flyable air = AircraftFactory.newAircraft(word[0], word[1],
																		Integer.parseInt(word[2]),
																		Integer.parseInt(word[3]),
																		Integer.parseInt(word[4]));
						airs.add(air);
					}
					catch (NumberFormatException e)
					{
						throw new SimulationFileException("Invalid simulation file: integer expected.");
					}
				}

				for (Flyable air : airs)
					air.registerTower(weatherTower);

				for (int i = 1; i <= simulations; i++)
					weatherTower.changeWeather();
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Error: Couldn't find file " + args[0]);
		}
		catch (AvajLauncherException e)
		{
			System.out.println("Error in AvajLauncherException: " + e.getMessage());
		}
		catch (IOException e)
		{
			System.out.println("Error while reading the file " + args[0]);
		}
		finally
		{
			Reading.gett().close();
		}
	}
}
