package src;

import src.UnknownWeatherException;
import src.WeatherTower;

/**
 * Created by nmatushe
 **/

public interface Flyable
{
    void updateConditions() throws UnknownWeatherException;

    void registerTower(WeatherTower weatherTower);
}