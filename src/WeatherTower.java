package src;

import src.UnknownWeatherException;
import src.Coordinates;

/**
 * Created by nmatushe
 **/

public class WeatherTower extends Tower
{
	public String getWeather(Coordinates coordinates)
	{
		return WeatherProvider.getProvider().getCurrentWeather(coordinates);
	}

	void changeWeather() throws UnknownWeatherException
	{
		WeatherProvider.getProvider().changeWeather();
		super.conditionsChanged();
	}
}